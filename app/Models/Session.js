'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Session extends Model {
    
    user () {
        return this.belongsTo('App/Models/User')
    }
    times () {
      return this.hasMany('App/Models/Time')
    }
}

module.exports = Session
