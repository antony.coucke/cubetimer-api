'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Time extends Model {
    session () {
      return this.belongsTo('App/Models/Session')
    }
}

module.exports = Time
