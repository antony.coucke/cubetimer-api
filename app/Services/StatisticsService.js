'use strict'

class StatisticsService {
    constructor(times) {
        this.times = times;
        this.setDurations();
    }

    setDurations() {
        this.durations = [];
        for (var time of this.times) {
            this.durations.push(time.duration);
        }
        this.durations.sort(); 
    }
    
    getMo3() {
        this.setDurations();
        let mean = null;
        if(this.durations.length >= 3) {  
            const durations = this.durations.reverse().slice(-3);           
            if(durations.indexOf(10000000000) >= 0) {
                return 10000000000;
            }  
            for(var duration of durations) {
                mean += duration;
            }         
            mean /= 3;
        }
        return mean;
    }

    getAverage() {
        let durations = this.durations.slice();
        durations.sort();
        let excludedLength = Math.ceil(durations.length * 0.05);
        let average = 0;
        if (durations.length > 3) {        
            durations.splice(0, excludedLength);
            durations = durations.reverse().slice();
            durations.splice(0, excludedLength); 
            if(durations.indexOf(10000000000) >= 0) {
                return 10000000000;
            }
            for (var i = 0; i < durations.length; i++) {
                average += durations[i];
            }
            return average /= durations.length;
        }
        return null;
    }

    getAo5() {
        this.setDurations();
        let average = null;
        if(this.durations.length >= 5) {
            this.durations = this.durations.reverse().slice(-5);
            average = this.getAverage();
        }
        return average;
    }

    getAo10() {
        this.setDurations();
        let average = null;
        if(this.durations.length >= 10) {
            this.durations = this.durations.reverse().slice(-10);
            average = this.getAverage();
            this.setDurations();
        }
        return average;
    }

    getAo30() {
        this.setDurations();
        let average = null;
        if(this.durations.length >= 30) {
            this.durations = this.durations.reverse().slice(-30);
            average = this.getAverage();
            this.setDurations();
        }
        return average;
    }
    
    getAo50() {
        this.setDurations();
        let average = null;
        if(this.durations.length >= 50) {
            this.durations = this.durations.reverse().slice(-50);
            average = this.getAverage();
            this.setDurations();
        }
        return average;
    }
    
    getAo100() {
        this.setDurations();
        let average = null;
        if(this.durations.length >= 100) {
            this.durations = this.durations.reverse().slice(-100);
            average = this.getAverage();
            this.setDurations();
        }
        return average;
    }

    getBestTime() {
        if(this.durations.length > 1) {
            let durations = this.durations.slice();
            durations.sort(); 
            return durations[0]; 
        }
        return null;
    }

    getWorstTime() {
        if(this.durations.length > 1) {
            let durations = this.durations.slice();            
            this.removeDNF(durations)
            durations.reverse(); 
            return durations[0]; 
        }
        return null;
    }

    removeDNF(durations) {
        for(var i in durations) {
            durations[i] === 10000000000 ? durations.splice(i,1) && this.removeDNF(durations) : false;
        }
    }

    setGlobalStatistics(session) {   
        session.average = this.getAverage();
        session.worst =  this.getWorstTime();
        session.best = this.getBestTime();
        session.mo3 = this.getMo3();
        session.ao5 = this.getAo5();
        session.ao10 = this.getAo10();
        session.ao30 = this.getAo30();
        session.ao50 = this.getAo50();
        session.ao100 = this.getAo100();
        return session;
    }
}
module.exports = StatisticsService