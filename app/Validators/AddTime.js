'use strict'

class AddTime {
  get rules () {
    return {
      session_id: 'required|number',
      duration: 'required|number',
      scramble: 'required|string',
    }
  }
  
  get messages () {
    return {
      'duration.required': 'Aucun temps envoyé',
      'duration.number': 'Le temps envoyé doit être au format float',
      'session_id.required': 'Vous devez renseigner un numéro de session pour ajouter le temps à cette dernière',
      'session_id.number': 'Le numéro de session n\'est pas un nombre valide',
      'scramble.required': 'Aucun mélange envoyé',
      'scramble.number': 'Le mélange envoyé doit être au format texte'
    }
  }  
}

module.exports = AddTime
