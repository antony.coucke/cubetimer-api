'use strict'

class RegisterUser {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      email: 'required|email|unique:users',
      username: 'required|unique:users|min:4|max:18',
      password: 'required|min:6|max:20|same:passwordConfirm',
      passwordConfirm: 'required|same:password'
    }
  }

  get messages () {
    return {
      'email.required': 'Please enter a valid e-mail address',
      'email.email': 'Please enter a valid e-mail address',
      'email.unique': 'E-mail adress already exists',
      'username.required': 'Please enter an username',
      'username.unique': 'The username you typed already exists',
      'username.min': 'Username must be between 4 and 18 characters long',
      'username.max': 'Username must be between 4 and 18 characters long',
      'password.required': 'Please enter a password',
      'password.min': 'Password must be between 6 and 20 characters long',
      'password.max': 'Password must be between 6 and 20 characters long',
      'password.same': 'Passwords don\'t match together',
      'passwordConfirm.required': 'You have to type password confirmation',
      'passwordConfirm.same': 'Passwords don\'t match together' 
    }
  }    
}

module.exports = RegisterUser
