'use strict'

class ScrambleController {    
    get({ request, response }) {
        let scramble = [];
        scramble.push(this.getRandomMove());
        for (var i = 0; i <= 20; i++) {
            this.scramble(scramble);
        }
        return response.json(scramble);
    }



    scramble(scramble) {
        this.addMoveToScramble(scramble);
        return scramble;
    }

    getForbiddenMoves(face) {
        return [
            face,
            face + "'",
            face + "2",
        ];
    }
    addMoveToScramble(scramble) {
        let opposites = [];
        opposites['U'] = 'D';
        opposites['L'] = 'R';
        opposites['F'] = 'B';
        opposites['D'] = 'U';
        opposites['R'] = 'L';
        opposites['B'] = 'F';
        var random = this.getRandomMove();
        //on vire les mouvements de la même face 
        var forbiddenMoves = this.getForbiddenMoves(random[0]).concat(this.getForbiddenMoves(opposites[random[0]]));
        if (forbiddenMoves.indexOf(scramble[scramble.length - 1]) >= 0) {
            return this.addMoveToScramble(scramble);
        }
        scramble.push(random);
        return scramble;
    }

    getRandomMove() {
        let moves = [
            "U", "R", "L", "D", "B", "F",
            "U'", "R'", "L'", "D'", "B'", "F'",
            "U2", "R2", "L2", "D2", "B2", "F2"
        ];
        return moves[Math.floor(Math.random() * moves.length)];
    }

    convertToString(scramble) {
        return scramble.join(' ');
    }

}

module.exports = ScrambleController
