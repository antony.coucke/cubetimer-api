'use strict'
const User = use('App/Models/User')
const Hash = use('Hash')

class UserController {
    async register ({ request, response }) {
        var data = request.post();
        const user = new User()
        user.email = data.email
        user.password = data.password
        user.username = data.username
        await user.save()
    }

    async login({ auth,request,response }) {
        const user = await auth.getUser();
        response.json(user);
    }
}

module.exports = UserController
