'use strict'

const Session = use('App/Models/Session')
const User = use('App/Models/User')
const Time = use('App/Models/Time')

class SessionController {
    async add({ auth, request, response }) {
        const user = await User.find(auth.user.id)
        const session = new Session()
        await session.user().associate(user)
        response.json(session)
    }

    async delete({ auth, params, request, response }) {
        const session_id = params.id
        const session = await Session.find(session_id)
        if (session.user_id === auth.user.id) {
            await session.delete();
            let sessions = await Session.query().where('user_id', auth.user.id).with('times').fetch();
            response.json(sessions.toJSON())
        }
    }

    async get({ auth, params, request, response }) {
        const sessions = await Session.query().where('user_id', auth.user.id).with('times').fetch()
        response.json(sessions);
    }

    async reset({ auth, params, request, response }) {
        const times = await Time.query().where('session_id', params.id).delete();
        response.json(true)
    }

}

module.exports = SessionController
