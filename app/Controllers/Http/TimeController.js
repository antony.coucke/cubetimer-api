'use strict'
const Time = use('App/Models/Time')
const Session = use('App/Models/Session')
const StatisticsService = use('App/Services/StatisticsService')

class TimeController {
    async add ({ auth, request, response }) {
        const data = request.post();
        let session = await Session.query().where('id', data.session_id).with('times').first();
        if(session.toJSON().user_id === auth.user.id) {    
            const time = new Time()
            time.duration = data.duration
            time.scramble = data.scramble
            await session.times().save(time)    
            const statisticsService = new StatisticsService(session.toJSON().times.concat([time])); 
            session = statisticsService.setGlobalStatistics(session);
            await session.save();
            session = await Session.query().where('id', data.session_id).with('times').first();
            response.send(session);
        }
    }

    async delete({ auth, params, request, response }) {   
        const id = params.id;
        const time = await Time.find(id);
        await time.delete();
        let session = await time.session().with('times').fetch();        
        const statisticsService = new StatisticsService(session.toJSON().times); 
        session = statisticsService.setGlobalStatistics(session);
        await session.save();
        response.send(session);
    }

    async addPenalty({ auth, request, response }) {
        const data = request.post();        
        const time = await Time.find(data.time_id);
        if(data.penalty === '+2') {
            time.duration += 2.00;
            await time.save();
        }     
        if(data.penalty === 'DNF') {
            time.duration = 10000000000;
            await time.save();
        }   
        let session = await time.session().with('times').fetch();   
        const statisticsService = new StatisticsService(session.toJSON().times); 
        session = statisticsService.setGlobalStatistics(session);
        await session.save();
        response.send(session); 
    }
}

module.exports = TimeController
