'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessionsSchema extends Schema {
  up () {
    this.table('sessions', (table) => {
      table.float('mo3')
    })
  }

  down () {
    this.table('sessions', (table) => {
      table.dropColumn('mo3')
    })
  }
}

module.exports = SessionsSchema
