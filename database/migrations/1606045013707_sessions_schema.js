'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessionsSchema extends Schema {
  up () {
    this.table('sessions', (table) => {
      table.float('best')
      table.float('worst')
      table.float('average')
      table.float('ao5')
      table.float('ao10')
      table.float('ao30')
      table.float('ao50')
      table.float('ao100')
    })
  }

  down () {
    this.table('sessions', (table) => {
      table.dropColumn('best')
      table.dropColumn('worst')
      table.dropColumn('average')
      table.dropColumn('ao5')
      table.dropColumn('ao10')
      table.dropColumn('ao30')
      table.dropColumn('ao50')
      table.dropColumn('ao100')
    })
  }
}

module.exports = SessionsSchema
