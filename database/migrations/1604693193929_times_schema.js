'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TimesSchema extends Schema {
  up () {
    this.create('times', (table) => {
      table.increments()      
      table.float('duration').notNullable()
      table.integer('session_id').unsigned().references('id').inTable('sessions').onDelete('CASCADE')
      table.string('scramble').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('times')
  }
}

module.exports = TimesSchema
