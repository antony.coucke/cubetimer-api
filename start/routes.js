'use strict'

const { RouteResource } = require('@adonisjs/framework/src/Route/Manager')

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.get('/testAPI', async ({ response }) => {
    response.send(true)
})

Route.group(() => {
    Route.post('add', 'TimeController.add').validator('AddTime').middleware('auth'),
    Route.post('addPenalty', 'TimeController.addPenalty').middleware('auth'),
    Route.delete('delete/:id', 'TimeController.delete').middleware('auth')
}).prefix('time')


Route.group(() => {
    Route.get('get/:id?', 'SessionController.get').middleware('auth'),
    Route.post('add', 'SessionController.add').middleware('auth'),
    Route.delete('delete/:id', 'SessionController.delete').middleware('auth'),
    Route.delete('reset/:id', 'SessionController.reset').middleware('auth')
}).prefix('session')



Route.group(() => {
    Route.post('register', 'UserController.register').middleware('guest').validator('RegisterUser'),
    Route.post('login', 'UserController.login').middleware('auth')
}).prefix('user')


Route.group(() => {
    Route.get('get', 'ScrambleController.get')
}).prefix('scramble')
